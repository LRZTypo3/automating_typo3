#! /bin/bash
################################################################################
# DIESES SKRIPT KLONT DIE PRODUKTIVE BADW-PROJEKTE-TYPO3-INSTANZ (di25lor)
# AUF DIE STAGING-INSTANZ (di25lon)
################################################################################

# Sanity-Check: Läuft das Skript unter root?
if [[ $EUID -ne 0 ]]
then
  echo "Dieses Skript muss als root aufgerufen werden!"
  exit 1
fi

########################################
# VARIABLEN SETZEN 
########################################
echo "Variablen werden gesetzt"
# src => www.typo3.badw.de / dst => staging.typo3.badw.de
kennung_src=xxx
kennung_dst=xxx
last_src=${kennung_src: -1}
last_dst=${kennung_dst: -1}
home_src=/nfs/web_tum_typo3/www/$last_src/$kennung_src
home_dst=/nfs/web_tum_typo3/www/$last_dst/$kennung_dst
htdocs_src=${home_src}/webserver/htdocs
htdocs_dst=${home_dst}/webserver/htdocs 
rel_conf_path=typo3conf/LocalConfiguration.php 

# PHP 5.3 kommt mit manchen Localconfiguration-Syntax-Elementen nicht zurecht
php_cli="/usr/local/bin/php" # Momentan 7.0

# Mit diesem Hack bekommen wir die MySQL-Credentials, die auch die jeweilige Typo3-Instanz nutzt:
# LocalConfiguration.php gibt als Datei ein PHP-Array mit der Config zurück
# dies machen wir uns zu nutze und geben das PW aus.
# Shell-Escaping beachten (Doppelt für Backticks und die doppelten Anführungszeichen)
db_src=`${php_cli} -r "\\\$c = require '${htdocs_src}/${rel_conf_path}' ; echo \\\$c['DB']['database'];"` 
db_dst=`${php_cli} -r "\\\$c = require '${htdocs_dst}/${rel_conf_path}' ; echo \\\$c['DB']['database'];"`
db_host_src=`${php_cli} -r "\\\$c = require '${htdocs_src}/${rel_conf_path}' ; echo \\\$c['DB']['host'];"`
db_host_dst=`${php_cli} -r "\\\$c = require '${htdocs_dst}/${rel_conf_path}' ; echo \\\$c['DB']['host'];"`
db_pw_src=`${php_cli} -r "\\\$c = require '${htdocs_src}/${rel_conf_path}' ; echo \\\$c['DB']['password'];"`
db_pw_dst=`${php_cli} -r "\\\$c = require '${htdocs_dst}/${rel_conf_path}' ; echo \\\$c['DB']['password'];"`

########################################
# VORARBEITEN VOR DEM KLONEN
######################################## 
echo "Vorarbeiten vor dem Klonen"
# Wegsichern der LocalConfiguration der staging-Instanz
cp -p ${htdocs_dst}/${rel_conf_path} ${home_dst}/webserver/LocalConfiguration.php_bkp

# Für alle DB-Aktionen muss eine .my.cnf angelegt werden, damit die Abfragen Passwortlos funktionieren
cat <<EOMYCNF > ~/.my.cnf_src
[client]
host      = ${db_host_src} 
user      = ${kennung_src}
password  = ${db_pw_src} 

[mysqldump]
host      = ${db_host_src} 
user      = ${kennung_src}
password  = ${db_pw_src} 
EOMYCNF

chmod 600 ~/.my.cnf_src

cat <<EOMYCNF > ~/.my.cnf_dst
[client]
host      = ${db_host_dst} 
user      = ${kennung_dst}
password  = ${db_pw_dst}
database  = ${db_dst}
EOMYCNF

chmod 600 ~/.my.cnf_dst

# Check der DB-Verbindung
check_db_src=`echo "show databases;" | mysql --defaults-extra-file=~/.my.cnf_src|grep ${db_src}`
if [ "$check_db_src" != "$db_src" ]
then
  echo "$db_src nicht erreichbar oder MySQL-Credentials nicht korrekt ($check_db_src)" 
  exit 1;
fi

check_db_dst=`echo "show databases;" | mysql --defaults-extra-file=~/.my.cnf_dst|grep ${db_dst}` 
if [ "$check_db_dst" != "$db_dst" ]

then
  echo "$db_dst nicht erreichbar oder MySQL-Credentials nicht korrekt ($check_db_dst)"
  exit;
fi

########################################
# DB-KLON
######################################## 
echo "DB-Klon"
# Lösche alte Staging-DB
echo "DROP DATABASE ${db_dst}; CREATE DATABASE ${db_dst}" \
  | mysql --defaults-extra-file=~/.my.cnf_dst

# pipe mysqldump von produktiver DB in Staging-DB
mysqldump --defaults-extra-file=~/.my.cnf_src ${db_src}\
  | mysql --defaults-extra-file=~/.my.cnf_dst

########################################
# DATEISYSTEM-KLON
######################################## 
echo "Dateisystem Klon"
# Lösche altes Docroot
# da wir ein bisschen paranoid sind, testen wir, ob $htdocs_dst einen sinnvollen Wert
# hat, damit wir nicht rekursiv Dinge löschen, die wir nicht löschen wollen.
if [[ ${htdocs_dst} == *"${kennung_dst}/webserver/htdocs" ]]
then
  rm -r ${htdocs_dst}
fi


# Kopiere Dateien
rsync -a ${htdocs_src} "${home_dst}/webserver"
# zweiter Durchlauf
# Dieser ist nötig, falls beim ersten Durchlauf dynamisch erstellte Dateien
# (z.B. Autoload-Dateien) unfertig synchronisiert wurden.
rsync -a --delete ${htdocs_src} "${home_dst}/webserver"

# korrigiere Zugriffsrechte
chown -R ${kennung_dst}:aw001 ${htdocs_dst}

########################################
# NACHARBEITEN NACH DEM KLONEN
######################################## 
echo "Nacharbeiten nach dem Klonen"
# Installiere weggespeicherte LocalConfiguration.php wieder 
mv  ${home_dst}/webserver/LocalConfiguration.php_bkp ${htdocs_dst}/${rel_conf_path} 
 
# Lösche den Cache auf dem Ziel
rm -fr ${htdocs_dst}/typo3temp/Cache

# Lösche DB-Zugangsinformationen
rm ~/.my.cnf_dst ~/.my.cnf_src 
