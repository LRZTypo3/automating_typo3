# Einfache Installation

```bash
COMPOSER=simple_composer.json composer install
```

# Installation mit separatem Web-Root: 
```bash
COMPOSER=web_dir_composer.json composer install
```
