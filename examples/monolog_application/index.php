<?php
    require __DIR__ . '/vendor/autoload.php';

    $log = new Monolog\Logger('lrztest');
    $log->pushHandler(
        new Monolog\Handler\StreamHandler(
            'lrztest.log',
            Monolog\Logger::WARNING
        )
    );
    $log->addWarning("It's that simple!");
