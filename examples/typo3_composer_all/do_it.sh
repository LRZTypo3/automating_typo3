#!/bin/bash

COMPOSER=composer_simple.json composer install
./vendor/helhum/typo3-console/Scripts/typo3cms install:setup \
  --non-interactive \
  --database-user-name="root" \
  --database-host-name="localhost" \
  --database-port="3306" \
  --databasename="typo3test01" \
  --database-user-password="zdado34u" \
  --database-create=0 \
  --admin-user-name="admin" \
  --admin-password="password" \
  --site-name="LRZTYPO3-DEMO"
